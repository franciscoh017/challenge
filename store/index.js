export const state = () => ({
  isDrawerOpen: true,
  shifts: [
    {
      title: 'Shift 1',
      description: 'This is my shift description',
      dates: [
        {
          day: '12 dec 2020',
          startTime: '09:00',
          endTime: '17:00',
          type: 'Consultation',
          price: 10
        },
        {
          day: '13 dec 2020',
          startTime: '08:00',
          endTime: '20:00',
          type: 'Telephone',
          price: 80
        },
      ]
    },
    {
      title: 'Shift 2',
      description: 'This is my shift 2 description',
      dates: [
        {
          day: '12 dec 2020',
          startTime: '20:00',
          endTime: '23:59',
          type: 'Telephone',
          price: 20
        },
      ]
    }
  ],
});

export const mutations = {
  ADD_SHIFT(state, shift) {
    state.shifts.push(shift);
  },
  EDIT_SHIFT(state, payload) {
    state.shifts[payload.index] = payload.shift;
  },
  DELETE_SHIFT(state, index)  {
    state.shifts.splice(index, 1);
  },
  TOGGLE_DRAWER(state) {
    state.isDrawerOpen = !state.isDrawerOpen;
  }
}

export const actions = {
  addShift({commit}, shift){
    commit('ADD_SHIFT', shift);
  },
  deleteShift({commit}, index){
    commit('DELETE_SHIFT', {index});
  },
  editShift({commit}, index, shift){
    commit('EDIT_SHIFT', {index, shift});
  },
  toggleDrawer({commit}) {
    commit('TOGGLE_DRAWER');
  },
}

export const getters = {
  filterLimits: (state) => {
    const limits = [].concat.apply([], state.shifts.map((shift) => shift.dates.map(date => date.price)));
    return {
      min: Math.min(...limits),
      max: Math.max(...limits),
    }
  }
}
